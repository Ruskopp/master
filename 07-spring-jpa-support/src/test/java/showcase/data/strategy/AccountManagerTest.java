package showcase.data.strategy;

import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringRunner.class)
@SpringBootTest
class AccountManagerTest {
    private static final double AMOUNT_TO_TRANSFER = 100;
    @Autowired
    private AccountManager manager;
    @Autowired
    private EntityManager entityManager;
    private Statistics statistics;

    private AccountManager.AccountLogger failingLogger = (p) -> {
        throw new RuntimeException("Logging failed");
    };
    private AccountManager.AccountLogger workingLogger = System.out::println;

    @PostConstruct
    void setUp() {
        statistics = entityManager.getEntityManagerFactory().unwrap(SessionFactory.class).getStatistics();
        statistics.setStatisticsEnabled(true);
    }

    @BeforeEach
    void clearStatistics() {
        statistics.clear();
    }

    @Test
    void transferMoneySuccess() {
        //Given
        /* Since this is executed in transaction with working logger we expect all changes to
            be successful.*/
        manager.setLogger(workingLogger);
        double expectedBalanceSource = manager.getBalanceForAccount(1) - AMOUNT_TO_TRANSFER;
        double expectedBalanceDestination = manager.getBalanceForAccount(2) + AMOUNT_TO_TRANSFER;
        int expectedTransferLogCountSource = manager.getTransferLogCountForAccount(1) + 1;
        int expectedTransferLogCountDestination = manager.getTransferLogCountForAccount(2) + 1;

        assertTransactionCountIs(4);

        //When
        manager.transferMoneyInsideTransaction(1, 2, AMOUNT_TO_TRANSFER);

        //Then
        assertTransactionCountIs(5);

        assertEquals(expectedBalanceSource, manager.getBalanceForAccount(1));
        assertEquals(expectedBalanceDestination, manager.getBalanceForAccount(2));
        assertEquals(expectedTransferLogCountSource, manager.getTransferLogCountForAccount(1));
        assertEquals(expectedTransferLogCountDestination, manager.getTransferLogCountForAccount(2));
    }


    @Test
    void transferMoneyFail_withTransaction() {
        //Given
        /* This is executed in transaction we expect all changes
           to rollback so that balances are same as in beginning. */
        manager.setLogger(failingLogger);
        double expectedBalanceSource = manager.getBalanceForAccount(1);
        double expectedBalanceDestination = manager.getBalanceForAccount(2);
        int expectedTransferLogCountSource = manager.getTransferLogCountForAccount(1);
        int expectedTransferLogCountDestination = manager.getTransferLogCountForAccount(2);

        assertSuccessfulTransactionCountIs(4);
        //When
        assertThrows(RuntimeException.class, () -> manager.transferMoneyInsideTransaction(1, 2, AMOUNT_TO_TRANSFER));

        //Then
        assertTransactionCountIs(5);
        assertSuccessfulTransactionCountIs(4);

        assertEquals(expectedBalanceSource, manager.getBalanceForAccount(1));
        assertEquals(expectedBalanceDestination, manager.getBalanceForAccount(2));
        assertEquals(expectedTransferLogCountSource, manager.getTransferLogCountForAccount(1));
        assertEquals(expectedTransferLogCountDestination, manager.getTransferLogCountForAccount(2));
    }

    @Test
    void transferMoneyFail_withoutTransaction() {
        //Given
        /* This is executed without transaction we expect first account to be changed
           and second to stay in initial state, which is inconsistent situation. */
        manager.setLogger(failingLogger);
        double expectedBalanceSource = manager.getBalanceForAccount(1) - AMOUNT_TO_TRANSFER;
        double expectedBalanceDestination = manager.getBalanceForAccount(2);
        int expectedTransferLogCountSource = manager.getTransferLogCountForAccount(1) + 1;
        int expectedTransferLogCountDestination = manager.getTransferLogCountForAccount(2);

        assertSuccessfulTransactionCountIs(4);

        //When
        assertThrows(Throwable.class, () -> manager.transferMoney(1, 2, AMOUNT_TO_TRANSFER));

        //Then
        assertTransactionCountIs(7);
        assertSuccessfulTransactionCountIs(7);

        assertEquals(expectedBalanceSource, manager.getBalanceForAccount(1));
        assertEquals(expectedBalanceDestination, manager.getBalanceForAccount(2));
        assertEquals(expectedTransferLogCountSource, manager.getTransferLogCountForAccount(1));
        assertEquals(expectedTransferLogCountDestination, manager.getTransferLogCountForAccount(2));
    }

    private void assertTransactionCountIs(int expectedTransactionCount) {
        assertEquals(expectedTransactionCount, statistics.getTransactionCount());
    }

    private void assertSuccessfulTransactionCountIs(int expectedTransactionCount) {
        assertEquals(expectedTransactionCount, statistics.getSuccessfulTransactionCount());
    }

}