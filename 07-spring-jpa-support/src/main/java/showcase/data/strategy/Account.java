package showcase.data.strategy;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String description;
    private BigDecimal balance;

    @OneToMany(mappedBy = "account")
    private List<TransferLog> transferLogs = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public List<TransferLog> getTransferLogs() {
        return transferLogs;
    }

    public void setTransferLogs(List<TransferLog> transferLogs) {
        this.transferLogs = transferLogs;
    }

    public void addTransaction(TransferLog transferLog) {
        transferLogs.add(transferLog);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", balance=" + balance +
                ", transactionsSize=" + transferLogs.size() +
                '}';
    }
}
