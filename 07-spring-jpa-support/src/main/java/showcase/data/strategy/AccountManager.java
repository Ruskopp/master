package showcase.data.strategy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Component
public class AccountManager {

    private final AccountRepository accountRepo;
    private final TransferLogRepository transferLogRepo;
    private AccountLogger logger;

    @FunctionalInterface
    public interface AccountLogger {
        void log(String s);
    }

    @Autowired
    public AccountManager(AccountRepository accountRepo, TransferLogRepository transferLogRepo) {
        this.accountRepo = accountRepo;
        this.transferLogRepo = transferLogRepo;
    }


    public void setLogger(AccountLogger logger) {
        this.logger = logger;
    }

    public double getBalanceForAccount(int accountId) {
        return accountRepo.findById(accountId)
                .map(acc -> acc.getBalance().doubleValue())
                .orElse(0.);
    }

    public int getTransferLogCountForAccount(int accountId) {
        return Long.valueOf(transferLogRepo
                                    .findAll().stream()
                                    .filter(tl -> tl.getAccount().getId() == accountId)
                                    .count())
                .intValue();
    }

    @Transactional
    public void transferMoneyInsideTransaction(int sourceAccountId, int destinationAccountId, double amount) {
        transferMoney(sourceAccountId, destinationAccountId, amount);
    }

    public void transferMoney(int sourceAccountId, int destinationAccountId, double amount) {
        withdrawalFromAccount(sourceAccountId, amount);
        logger.log("Money is withdrawn from account " + sourceAccountId);

        depositToAccount(destinationAccountId, amount);
        logger.log("Money is deposited to account " + destinationAccountId);

    }

    private void withdrawalFromAccount(int sourceAccountId, double amount) {
        Account account = accountRepo.findById(sourceAccountId)
                .orElseThrow(() -> new RuntimeException("Account not found " + sourceAccountId));

        double newAmount = account.getBalance().doubleValue() - amount;
        account.setBalance(BigDecimal.valueOf(newAmount));
        account.setDescription(account.getDescription());

        TransferLog transferLog = new TransferLog();
        transferLog.setTimeStamp(LocalDateTime.now());
        transferLog.setAmount(BigDecimal.valueOf(amount));
        transferLog.setType("WITHDRAWAL");
        transferLog.setAccount(account);
        accountRepo.save(account);
        transferLogRepo.save(transferLog);
    }

    private void depositToAccount(int destinationAccountId, double amount) {
        Account account = accountRepo.findById(destinationAccountId)
                .orElseThrow(() -> new RuntimeException("Account not found " + destinationAccountId));

        double newAmount = account.getBalance().doubleValue() + amount;
        account.setBalance(BigDecimal.valueOf(newAmount));

        TransferLog transferLog = new TransferLog();
        transferLog.setTimeStamp(LocalDateTime.now());
        transferLog.setAmount(BigDecimal.valueOf(amount));
        transferLog.setType("DEPOSIT");
        transferLog.setAccount(account);
        accountRepo.save(account);
        transferLogRepo.save(transferLog);
    }
}
