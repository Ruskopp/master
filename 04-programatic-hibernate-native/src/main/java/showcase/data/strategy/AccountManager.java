package showcase.data.strategy;


import org.hibernate.Session;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@SuppressWarnings("Duplicates")
public class AccountManager {
    private Session session;
    private AccountLogger logger;

    @FunctionalInterface
    public interface AccountLogger {
        void log(String s);
    }

    public AccountManager(Session session) {
        this.session = session;
    }

    public AccountManager() {
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void setLogger(AccountLogger logger) {
        this.logger = logger;
    }

    public double getBalanceForAccount(int accountId) {
        return session
                .byId(Account.class)
                .loadOptional(accountId)
                .map(acc -> acc.getBalance().doubleValue())
                .orElse(0.);
    }

    public int getTransferLogCountForAccount(int accountId) {
        return Long.valueOf(session.createQuery("from TransferLog ", TransferLog.class).list().stream()
                                    .filter(tl -> tl.getAccount().getId() == accountId)
                                    .count()).intValue();
    }

    public void transferMoneyInsideTransaction(int sourceAccountId, int destinationAccountId, double amount) {
        try {
            session.getTransaction().begin();
            transferMoney(sourceAccountId, destinationAccountId, amount);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            throw new RuntimeException(e);
        }
    }

    public void transferMoney(int sourceAccountId, int destinationAccountId, double amount) {
        withdrawalFromAccount(sourceAccountId, amount);
        logger.log("Money is withdrawn from account " + sourceAccountId);

        depositToAccount(destinationAccountId, amount);
        logger.log("Money is deposited to account " + destinationAccountId);

    }

    private void withdrawalFromAccount(int sourceAccountId, double amount) {
        Account account = session
                .byId(Account.class)
                .loadOptional(sourceAccountId)
                .orElseThrow(() -> new RuntimeException("Account not found"));

        double newAmount = account.getBalance().doubleValue() - amount;
        account.setBalance(BigDecimal.valueOf(newAmount));

        TransferLog transferLog = new TransferLog();
        transferLog.setTimeStamp(LocalDateTime.now());
        transferLog.setAmount(BigDecimal.valueOf(amount));
        transferLog.setType("WITHDRAWAL");
        transferLog.setAccount(account);

        session.save(transferLog);
    }

    private void depositToAccount(int destinationAccountId, double amount) {
        Account account = session
                .byId(Account.class)
                .loadOptional(destinationAccountId)
                .orElseThrow(() -> new RuntimeException("Account not found"));

        double newAmount = account.getBalance().doubleValue() + amount;
        account.setBalance(BigDecimal.valueOf(newAmount));

        TransferLog transferLog = new TransferLog();
        transferLog.setTimeStamp(LocalDateTime.now());
        transferLog.setAmount(BigDecimal.valueOf(amount));
        transferLog.setType("DEPOSIT");
        transferLog.setAccount(account);

        session.save(transferLog);
    }
}
