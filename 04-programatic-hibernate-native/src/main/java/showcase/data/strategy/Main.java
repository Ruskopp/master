package showcase.data.strategy;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;

public class Main {
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    private static SessionFactory sessionFactory;

    static {
        // A SessionFactory is set up once for an application!
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() // configures settings from hibernate.cfg.xml
                .build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

    public static void main(String[] args) {
        System.out.println(sessionFactory.isOpen());

        Session session = sessionFactory.openSession();
        session.beginTransaction();
        // session.save( new Event( "Our very first event!", new Date() ) );
        // session.save( new Event( "A follow up event", new Date() ) );
        session.getTransaction().commit();
        session.close();

        session = sessionFactory.openSession();
        List accounts = session.createQuery("from Account ").list();
        for (Account account : (List<Account>) accounts) {
            System.out.println(account);
        }
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        List transactions = session.createQuery("from TransferLog ").list();
        for (TransferLog transferLog : (List<TransferLog>) transactions) {
            System.out.println(transferLog);
        }
        session.flush();
        session.close();
    }

}
