package showcase.data.strategy;

import com.mysql.cj.jdbc.MysqlXADataSource;
import org.postgresql.xa.PGXADataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.jta.bitronix.BitronixXADataSourceWrapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@SpringBootApplication
@Configuration
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Bean
    @Qualifier("dataSourceMySql")
    public DataSource dataSource() throws Exception {
        MysqlXADataSource dataSource = new MysqlXADataSource();
        dataSource.setUrl("jdbc:mysql://localhost:3306/master");
        dataSource.setUser("root");
        dataSource.setPassword("root");
        BitronixXADataSourceWrapper wrapper = new BitronixXADataSourceWrapper();
        return wrapper.wrapDataSource(dataSource);
    }

    @Bean
    @Qualifier("dataSourcePostres")
    public DataSource dataSourceAudit() throws Exception {
        PGXADataSource dataSource = new PGXADataSource();
        dataSource.setServerName("localhost");
        dataSource.setPortNumber(5432);
        dataSource.setDatabaseName("postgres");
        dataSource.setUser("postgres");
        dataSource.setPassword("root");
        BitronixXADataSourceWrapper wrapper = new BitronixXADataSourceWrapper();
        return wrapper.wrapDataSource(dataSource);
    }

    @Bean
    @Qualifier("jdbcTemplateMySql")
    public JdbcTemplate jdbcTemplate(@Qualifier("dataSourceMySql") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    @Qualifier("jdbcTemplatePostres")
    public JdbcTemplate jdbcTemplateAudit(@Qualifier("dataSourcePostres") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
