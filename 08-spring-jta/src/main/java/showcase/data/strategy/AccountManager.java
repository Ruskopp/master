package showcase.data.strategy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class AccountManager {

    private final JdbcTemplate accountTemplate;
    private final JdbcTemplate transferLogTemplate;
    private AccountLogger logger;


    @FunctionalInterface
    public interface AccountLogger {
        void log(String s);
    }

    @Autowired
    public AccountManager(@Qualifier("jdbcTemplateMySql") JdbcTemplate accountTemplate,
                          @Qualifier("jdbcTemplatePostres") JdbcTemplate transferLogTemplate) {
        this.accountTemplate = accountTemplate;
        this.transferLogTemplate = transferLogTemplate;
    }

    public void setLogger(AccountLogger logger) {
        this.logger = logger;
    }

    public double getBalanceForAccount(long accountId) {
        return accountTemplate
                .query("SELECT * FROM account WHERE id =" + accountId + "",
                       (rs, i) -> rs.getDouble("balance")).get(0);
    }

    public int getTransferLogCountForAccount(long accountId) {
        return transferLogTemplate
                .queryForObject("SELECT count(*) FROM transfer_log WHERE account_id =" + accountId + "", Integer.class);
    }

    @Transactional
    public void transferMoneyInsideTransaction(long sourceAccountId, long destinationAccountId, double amount) {
        transferMoney(sourceAccountId, destinationAccountId, amount);
    }

    public void transferMoney(long sourceAccountId, long destinationAccountId, double amount) {
        withdrawalFromAccount(sourceAccountId, amount);
        logger.log("Money is withdrawn from account " + sourceAccountId);

        depositToAccount(destinationAccountId, amount);
        logger.log("Money is deposited to account " + destinationAccountId);
    }

    private void withdrawalFromAccount(long sourceAccountId, double amount) {
        double currentAmount = accountTemplate
                .query("SELECT * FROM account WHERE id =" + sourceAccountId + "",
                       (rs, i) -> rs.getDouble("balance")).get(0);

        double newAmount = currentAmount - amount;

        accountTemplate.execute("UPDATE account SET balance =" + newAmount + " WHERE id =" + sourceAccountId);
        transferLogTemplate.execute("INSERT INTO transfer_log (account_id, time_stamp, amount, type) VALUES ( " + sourceAccountId + ", LOCALTIMESTAMP, " + amount + ", 'WITHDRAWAL')");
    }

    private void depositToAccount(long destinationAccountId, double amount) {
        double currentAmount = accountTemplate
                .query("SELECT * FROM account WHERE id =" + destinationAccountId + "",
                       (rs, i) -> rs.getDouble("balance")).get(0);

        double newAmount = currentAmount + amount;

        accountTemplate.execute("UPDATE account SET balance =" + newAmount + " WHERE id =" + destinationAccountId);
        transferLogTemplate.execute("INSERT INTO transfer_log (account_id, time_stamp, amount, type) VALUES ( " + destinationAccountId + ", LOCALTIMESTAMP, " + amount + ", 'DEPOSIT')");
    }
}
