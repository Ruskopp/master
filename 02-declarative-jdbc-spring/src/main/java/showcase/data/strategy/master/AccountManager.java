package showcase.data.strategy.master;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class AccountManager {

    private final JdbcTemplate jdbcTemplate;
    private AccountLogger logger;

    @FunctionalInterface
    public interface AccountLogger {
        void log(String s);
    }

    @Autowired
    public AccountManager(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setLogger(AccountLogger logger) {
        this.logger = logger;
    }

    public double getBalanceForAccount(long accountId) {
        return jdbcTemplate
                .query("SELECT * FROM account WHERE id =" + accountId + "",
                       (rs, i) -> rs.getDouble("balance")).get(0);
    }

    public int getTransferLogCountForAccount(long accountId) {
        return jdbcTemplate
                .queryForObject("SELECT count(*) FROM transfer_log WHERE account_id =" + accountId + "", Integer.class);
    }

    @Transactional
    public void transferMoneyInsideTransaction(long sourceAccountId, long destinationAccountId, double amount) {
        transferMoney(sourceAccountId, destinationAccountId, amount);
    }

    public void transferMoney(long sourceAccountId, long destinationAccountId, double amount) {
        withdrawalFromAccount(sourceAccountId, amount);
        logger.log("Money is withdrawn from account " + sourceAccountId);

        depositToAccount(destinationAccountId, amount);
        logger.log("Money is deposited to account " + destinationAccountId);
    }

    private void withdrawalFromAccount(long sourceAccountId, double amount) {
        double currentAmount = jdbcTemplate
                .query("SELECT * FROM account WHERE id =" + sourceAccountId + "",
                       (rs, i) -> rs.getDouble("balance")).get(0);

        double newAmount = currentAmount - amount;

        jdbcTemplate.execute("UPDATE account SET balance =" + newAmount + " WHERE id =" + sourceAccountId);
        jdbcTemplate.execute("INSERT INTO transfer_log (account_id, time_stamp, amount, type) VALUES ( " + sourceAccountId + ", LOCALTIMESTAMP, " + amount + ", 'WITHDRAWAL')");
    }

    private void depositToAccount(long destinationAccountId, double amount) {
        double currentAmount = jdbcTemplate
                .query("SELECT * FROM account WHERE id =" + destinationAccountId + "",
                       (rs, i) -> rs.getDouble("balance")).get(0);

        double newAmount = currentAmount + amount;

        jdbcTemplate.execute("UPDATE account SET balance =" + newAmount + " WHERE id =" + destinationAccountId);
        jdbcTemplate.execute("INSERT INTO transfer_log (account_id, time_stamp, amount, type) VALUES ( " + destinationAccountId + ", LOCALTIMESTAMP, " + amount + ", 'DEPOSIT')");
    }

}
