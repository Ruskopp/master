package showcase.data.strategy.master;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Bean
    public DataSource configureDataSourceBean() {
        DataSourceBuilder<?> builder = DataSourceBuilder.create();

        builder.url("jdbc:mysql://localhost:3306/master");
        builder.username("user");
        builder.password("password");

        return builder.build();
    }

}
