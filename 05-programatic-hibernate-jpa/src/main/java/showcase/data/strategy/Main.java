package showcase.data.strategy;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Main {

    public static EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    private static EntityManagerFactory entityManagerFactory;

    static {
        entityManagerFactory = Persistence.createEntityManagerFactory("showcase.data.strategy");
    }

    public static void main(String[] args) {
        System.out.println(entityManagerFactory.isOpen());

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        // entityManager.save( new Event( "Our very first event!", new Date() ) );
        // entityManager.save( new Event( "A follow up event", new Date() ) );
        entityManager.getTransaction().commit();
        entityManager.close();

        entityManager = entityManagerFactory.createEntityManager();
        List<Account> accounts = entityManager.createQuery("from Account ", Account.class).getResultList();
        for (Account account : accounts) {
            System.out.println(account);
        }
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        List<TransferLog> transferLogs = entityManager.createQuery("from TransferLog ",
                                                                   TransferLog.class).getResultList();
        for (TransferLog transferLog : transferLogs) {
            System.out.println(transferLog);
        }
        entityManager.flush();
        entityManager.close();
    }

}
