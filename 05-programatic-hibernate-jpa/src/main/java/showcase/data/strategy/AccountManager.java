package showcase.data.strategy;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

@SuppressWarnings("Duplicates")
public class AccountManager {

    private EntityManager entityManager;
    private AccountLogger logger;

    @FunctionalInterface
    public interface AccountLogger {
        void log(String s);
    }

    public AccountManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public AccountManager() {
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setLogger(AccountLogger logger) {
        this.logger = logger;
    }

    public double getBalanceForAccount(int accountId) {
        return Optional
                .ofNullable(entityManager.find(Account.class, accountId))
                .map(acc -> acc.getBalance().doubleValue())
                .orElse(0.);
    }

    public int getTransferLogCountForAccount(long accountId) {
        return Long.valueOf(entityManager
                                    .createQuery("SELECT tl from TransferLog as tl", TransferLog.class)
                                    .getResultList().stream()
                                    .filter(transferLog -> transferLog.getAccount().getId() == accountId)
                                    .count())
                .intValue();
    }

    public void transferMoneyInsideTransaction(int sourceAccountId, int destinationAccountId, double amount) {
        try {
            entityManager.getTransaction().begin();
            transferMoney(sourceAccountId, destinationAccountId, amount);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
            throw new RuntimeException(e);
        }
    }

    public void transferMoney(int sourceAccountId, int destinationAccountId, double amount) {
        withdrawalFromAccount(sourceAccountId, amount);
        logger.log("Money is withdrawn from account " + sourceAccountId);

        depositToAccount(destinationAccountId, amount);
        logger.log("Money is deposited to account " + destinationAccountId);

    }

    private void withdrawalFromAccount(int sourceAccountId, double amount) {
        Account account = Optional
                .ofNullable(entityManager.find(Account.class, sourceAccountId))
                .orElseThrow(() -> new RuntimeException("Account not found"));

        double newAmount = account.getBalance().doubleValue() - amount;
        account.setBalance(BigDecimal.valueOf(newAmount));

        TransferLog transferLog = new TransferLog();
        transferLog.setTimeStamp(LocalDateTime.now());
        transferLog.setAmount(BigDecimal.valueOf(amount));
        transferLog.setType("WITHDRAWAL");
        transferLog.setAccount(account);

        entityManager.persist(transferLog);
    }

    private void depositToAccount(int destinationAccountId, double amount) {
        Account account = Optional
                .ofNullable(entityManager.find(Account.class, destinationAccountId))
                .orElseThrow(() -> new RuntimeException("Account not found"));

        double newAmount = account.getBalance().doubleValue() + amount;
        account.setBalance(BigDecimal.valueOf(newAmount));

        TransferLog transferLog = new TransferLog();
        transferLog.setTimeStamp(LocalDateTime.now());
        transferLog.setAmount(BigDecimal.valueOf(amount));
        transferLog.setType("DEPOSIT");
        transferLog.setAccount(account);

        entityManager.persist(transferLog);
    }
}
