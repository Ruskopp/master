package showcase.data.strategy;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SuppressWarnings("Duplicates")
class AccountManagerTest {
    private static final double AMOUNT_TO_TRANSFER = 100;
    private static final int SOURCE_ID = 1;
    private static final int DESTINATION_ID = 2;
    private AccountManager manager = new AccountManager();

    private AccountManager.AccountLogger failingLogger = (p) -> {
        throw new RuntimeException("Logging failed");
    };
    private AccountManager.AccountLogger workingLogger = System.out::println;


    @BeforeAll
    static void init() {
        Main.makeJDBCConnection();
    }

    @Test
    void transferMoneySuccess() {
        //Given
        /* This is executed in transaction with working logger we expect all changes to
           be successful. */
        manager.setLogger(workingLogger);
        double expectedBalanceSource = manager.getBalanceForAccount(SOURCE_ID) - AMOUNT_TO_TRANSFER;
        double expectedBalanceDestination = manager.getBalanceForAccount(DESTINATION_ID) + AMOUNT_TO_TRANSFER;
        int expectedTransferLogCountSource = manager.getTransferLogCountForAccount(SOURCE_ID) + 1;
        int expectedTransferLogCountDestination = manager.getTransferLogCountForAccount(DESTINATION_ID) + 1;

        //When
        manager.transferMoneyInsideTransaction(SOURCE_ID, DESTINATION_ID, AMOUNT_TO_TRANSFER);

        //Then
        assertEquals(expectedBalanceSource, manager.getBalanceForAccount(SOURCE_ID));
        assertEquals(expectedBalanceDestination, manager.getBalanceForAccount(DESTINATION_ID));
        assertEquals(expectedTransferLogCountSource, manager.getTransferLogCountForAccount(SOURCE_ID));
        assertEquals(expectedTransferLogCountDestination, manager.getTransferLogCountForAccount(DESTINATION_ID));
    }

    @Test
    void transferMoneyFail_withTransaction() {
        //Given
        /* This is executed in transaction we expect all changes
           to rollback so that balances are same as in beginning. */
        manager.setLogger(failingLogger);
        double expectedBalanceSource = manager.getBalanceForAccount(SOURCE_ID);
        double expectedBalanceDestination = manager.getBalanceForAccount(DESTINATION_ID);
        int expectedTransferLogCountSource = manager.getTransferLogCountForAccount(SOURCE_ID);
        int expectedTransferLogCountDestination = manager.getTransferLogCountForAccount(DESTINATION_ID);

        //When
        assertThrows(Throwable.class,
                     () -> manager.transferMoneyInsideTransaction(SOURCE_ID, DESTINATION_ID, AMOUNT_TO_TRANSFER));

        //Then
        assertEquals(expectedBalanceSource, manager.getBalanceForAccount(SOURCE_ID));
        assertEquals(expectedBalanceDestination, manager.getBalanceForAccount(DESTINATION_ID));
        assertEquals(expectedTransferLogCountSource, manager.getTransferLogCountForAccount(SOURCE_ID));
        assertEquals(expectedTransferLogCountDestination, manager.getTransferLogCountForAccount(DESTINATION_ID));
    }

    @Test
    void transferMoneyFail_withoutTransaction() {
        //Given
        /* This is executed without transaction we expect first account to be changed
           and second to stay in initial state, which is inconsistent situation. */
        manager.setLogger(failingLogger);
        double expectedBalanceSource = manager.getBalanceForAccount(SOURCE_ID) - AMOUNT_TO_TRANSFER;
        double expectedBalanceDestination = manager.getBalanceForAccount(DESTINATION_ID);
        int expectedTransferLogCountSource = manager.getTransferLogCountForAccount(SOURCE_ID) + 1;
        int expectedTransferLogCountDestination = manager.getTransferLogCountForAccount(DESTINATION_ID);

        //When
        assertThrows(Throwable.class, () -> manager.transferMoney(SOURCE_ID, DESTINATION_ID, AMOUNT_TO_TRANSFER));

        //Then
        assertEquals(expectedBalanceSource, manager.getBalanceForAccount(SOURCE_ID));
        assertEquals(expectedBalanceDestination, manager.getBalanceForAccount(DESTINATION_ID));
        assertEquals(expectedTransferLogCountSource, manager.getTransferLogCountForAccount(SOURCE_ID));
        assertEquals(expectedTransferLogCountDestination, manager.getTransferLogCountForAccount(DESTINATION_ID));
    }
}