package showcase.data.strategy;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountManager {

    private final Connection connection = Main.connection;
    private AccountLogger logger;

    @FunctionalInterface
    public interface AccountLogger {
        void log(String s);
    }

    public void setLogger(AccountLogger logger) {
        this.logger = logger;
    }

    public double getBalanceForAccount(long accountId) {
        try {
            ResultSet rs = connection.createStatement()
                    .executeQuery("SELECT * FROM account WHERE id =" + accountId + "");
            if (rs.next()) {
                return rs.getDouble("balance");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getTransferLogCountForAccount(long accountId) {
        ResultSet rs = null;
        try {
            rs = connection.createStatement()
                    .executeQuery("SELECT count(*) as transferLogCount FROM transfer_log WHERE account_id =" + accountId + "");

            if (rs.next()) {
                return rs.getInt("transferLogCount");
            } else {
                return 0;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error while getting TransferLogCount for accountId:" + accountId, e);
        }
    }

    public void transferMoneyInsideTransaction(long sourceAccountId, long destinationAccountId, double amount) {
        try {
            connection.setAutoCommit(false);
            transferMoney(sourceAccountId, destinationAccountId, amount);
            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }

            throw new RuntimeException(e);
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void transferMoney(long sourceAccountId, long destinationAccountId, double amount) {
        withdrawalFromAccount(sourceAccountId, amount);
        logger.log("Money is withdrawn from account " + sourceAccountId);

        depositToAccount(destinationAccountId, amount);
        logger.log("Money is deposited to account " + destinationAccountId);
    }

    private void withdrawalFromAccount(long sourceAccountId, double amount) {
        try {
            double currentAmount = 0;
            ResultSet rs = connection.createStatement()
                    .executeQuery("SELECT * FROM account WHERE id =" + sourceAccountId + "");
            if (rs.next()) {
                currentAmount = rs.getDouble("balance");
            }

            double newAmount = currentAmount - amount;

            connection.createStatement().execute("UPDATE account SET balance =" + newAmount + " WHERE id =" + sourceAccountId);
            connection.createStatement().execute(
                    "INSERT INTO transfer_log (account_id, time_stamp, amount, type) VALUES ( " + sourceAccountId + ", LOCALTIMESTAMP, " + amount + ", 'WITHDRAWAL')");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void depositToAccount(long destinationAccountId, double amount) {
        try {
            double currentAmount = 0;
            ResultSet rs = connection.createStatement()
                    .executeQuery("SELECT * FROM account WHERE id =" + destinationAccountId + "");

            if (rs.next()) {
                currentAmount = rs.getDouble("balance");
            }
            double newAmount = currentAmount + amount;

            connection.createStatement().execute("UPDATE account SET balance =" + newAmount + " WHERE id =" + destinationAccountId);
            connection.createStatement().execute(
                    "INSERT INTO transfer_log (account_id, time_stamp, amount, type) VALUES ( " + destinationAccountId + ", LOCALTIMESTAMP, " + amount + ", 'DEPOSIT')");
        } catch (SQLException e) {

        }
    }

}
