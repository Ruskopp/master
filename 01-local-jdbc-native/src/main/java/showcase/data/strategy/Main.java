package showcase.data.strategy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {
    static Connection connection;

    public static void main(String[] args) {

    }

    static void makeJDBCConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            log("MySQL JDBC driver loaded successfully.");
        } catch (ClassNotFoundException e) {
            log("MySQL JDBC driver not found.");
            e.printStackTrace();
            return;
        }

        try {
            // DriverManager: The basic service for managing a set of JDBC drivers.
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/master", "user", "password");
            if (connection != null) {
                log("Connection successful.");
            } else {
                log("Connection failed.");
            }
        } catch (SQLException e) {
            log("Connection failed with exception message: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static void log(String logMessage) {
        System.out.println(logMessage);
    }
}
